"use strict";

// Упражнение 1

let a = "$100"; 
let a1 = a.slice(1);
let a2 = Number(a1);

let b = "300$";
let b1 = b.substring(0, b.length-1);
let b2 = Number(b1);

let summ = a2 + b2;//Ваше решение
console.log(summ);// Должно быть 400

// Упражнение 2

let message1 = " привет, медвед     ";
let message2 = message1.trim();
let message3 = message2[0].toUpperCase() + message2.substring(1); 
console.log(message3); // “Привет, медвед”

// Упражнение 3

let age = prompt("Сколько вам лет?");

if ((age > 0) & (age <= 3)) {
  let message = `Вам ${age} лет и вы младенец`;
  alert(message);
} else if ((age >= 4) & (age <= 11)) {
  let message = `Вам ${age} лет и вы ребенок`;
  alert(message);
} else if ((age >= 12) & (age <= 18)) {
  let message = `Вам ${age} лет и вы подросток`;
  alert(message);
} else if ((age >= 19) & (age <= 40)) {
  let message = `Вам ${age} лет и вы познаете жизнь`;
  alert(message);
} else if ((age >= 41) & (age <= 80)) {
  let message = `Вам ${age} и вы познали жизнь`;
  alert(message);
} else if (age >= 81) {
  let message = `Вам ${age} и вы долгожитель`;
  alert(message);
}

// Упражнение 4

let str = "Я работаю со строками как профессионал!";
let count = str.split(" ");
console.log(count.length);
