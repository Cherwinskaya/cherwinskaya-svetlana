function showResult(content) {
    const result = document.createElement('div');
    result.id = 'result';
    result.className = 'bem-validator';
    result.style.position = 'absolute';
    result.style.top = '1em';
    result.style.right = '1em';
    result.style.bottom = '1em';
    result.style.left = '1em';
    result.style.border = '0.0125em solid #ccc';
    result.style.padding = '0';
    result.style.background = '#fefefe';
    
    const resultContent = document.createElement('div');
    resultContent.className = 'bem-validator__result';
    resultContent.innerHTML = content;
    
    const closeBtn = document.createElement('button');
    closeBtn.className = 'bem-validator__close-btn';
    closeBtn.style.position = 'absolute';
    closeBtn.style.top = '0.5em';
    closeBtn.style.right = '0.5em';
    closeBtn.innerText = 'X';
    closeBtn.addEventListener('click', () => result.remove());
    
    result.append(closeBtn, resultContent);
    
    document.body.prepend(result);
}

function checkBEM(){
  // get all elements in a page
    var allEls = document.body.querySelectorAll('*');
    // traverse through all elements and see if the follow the rules
    var logMsg = '<div class="resultpane"><h1 class="resultpane__header">BEM Inspect Results</h1><ol class="resultpane__list">';

    for(i = 0; i < allEls.length ; i++) {
        // first check if it's a classless div or span
        if((allEls[i].tagName == 'DIV' || allEls[i].tagName == 'SPAN') && (allEls[i].classList.length === 0)) {
            logMsg += '<li class="resultpane__item">Contains <code>' + allEls[i].tagName + '</code> elements with no class. These are unnecessary.</li>';
            allEls[i].style.outline='2px solid red';
            // no need to move on with this element
            continue;
        }
        // get classlist and make into array
        var elClassSet = Array.from(allEls[i].classList);

        // traverse through classes
        for(j = 0; j < elClassSet.length; j++) {
            // Is the class an Element?
            var element = elClassSet[j].indexOf('__') > -1;
            // yes
            if(element) {
                // check if it's inside the right block
                var elementClass = elClassSet[j].split('__')[0];
                var parentEl = allEls[i].parentNode;
                var isInBlock = false;

                while(parentEl.tagName != "HTML") {
                    var parentElClassSet = Array.from(parentEl.classList);
                    if(parentElClassSet.includes(elementClass)) {
                        isInBlock = true;
                        break;
                    }
                    parentEl = parentEl.parentNode;
                }

                if(!isInBlock) {
                    logMsg += '<li class="resultpane__item"><code>' + elClassSet[j] + '</code> is positioned outside the <strong>Block</strong> (<code>' + elClassSet[j].split('__')[0] + '</code>).</li>';
                    allEls[i].style.outline='2px solid red';
                }

                // check if it's double elemented ("x__y__z")
                var doubleElement = elClassSet[j].match(/[\w-]+__[\w-]+__[\w-]+/g);

                if(doubleElement != null) {
                    logMsg += '<li class="resultpane__item"><code>' + elClassSet[j] + '</code> is not a valid BEM class. Two <strong>Elements</strong> on the same class is not allowed.</li>';
                    allEls[i].style.outline='2px solid red';
                }
            }

            // does it contain a modifier?
            var modifier = elClassSet[j].indexOf('--') > -1;
            // yes
            if(modifier) {
                // if it's prefixed with "u-" skip
                if(!elClassSet[j].indexOf('u-') == 0) {
                    // double modifier
                    var doubleModifier = elClassSet[j].match(/[\w-]+--[\w-]+--[\w-]+/g);
                    if(doubleModifier != null) {
                        logMsg += '<li class="resultpane__item"><code>' + elClassSet[j] + '</code> is not a valid BEM class. Two <strong>Modifiers</strong> on the same class is not allowed.</li>';
                        allEls[i].style.outline='2px solid red';
                    }

                    // check if the default class is present ("element element--modifier")
                    var elementClass = elClassSet[j].split('--')[0];
                    if(!elClassSet.includes(elementClass)) {
                        logMsg += '<li class="resultpane__item"><code>' + elClassSet[j] + '</code> is included without it\'s default <strong>Element</strong> (<code>' + elementClass + '</code>).</li>';
                        allEls[i].style.outline='2px solid red';
                    }
                }
            }
        }
    }

    //output logmsg
    logMsg += '</ol></div>';
		  
    showResult(logMsg);
}

document.addEventListener('DOMContentLoaded', function() {
    const btn = document.createElement('button');
    btn.style.position = 'fixed';
    btn.style.bottom = '1em';
    btn.style.right = '1em';
    btn.innerText = 'Validate';
    btn.addEventListener('click', checkBEM);
    
    document.body.append(btn);
});
